class Library
  def initialize
    @books = []
  end

  def has_book?(book)
    @books.include?(book)
  end

  def checkout(book)
    @books.delete(book)
  end

  def return(book)
    @books << book
  end
end