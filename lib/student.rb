class Student
  def read(book, library = Library.new)
    if library.has_book?(book)
      library.checkout(book)
    end
  end
end