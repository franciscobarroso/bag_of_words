require_relative '../lib/library'

describe Library do
  verify_contract(:library)

  let(:library) { Library.new }

  it "allows checking out books that are in the inventory" do
    library.return("Moby Dick")

    expect(library.has_book?("Moby Dick")).to be_true
  end

  it "does not allow checking out unavailable books" do
    expect(library.has_book?("Moby Dick")).to be_false
  end

  it "marks books as unavailable after they are checked out" do
    library.return("Moby Dick")

    library.checkout("Moby Dick")

    expect(library.has_book?("Moby Dick")).to be_false
  end
end