require_relative '../lib/student'
require_relative '../lib/library'

describe Student do
  fake(:library)

  it "checks out the book from library if it is available" do
    student = Student.new
    mock(library).has_book?("Moby Dick") { true }
    mock(library).checkout("Moby Dick") { "Moby Dick" }

    student.read("Moby Dick", library)
  end

  it "does not check out the book from library if not available" do
    student = Student.new
    mock(library).has_book?("Moby Dick") { false }

    student.read("Moby Dick", library)

    expect(library).not_to have_received.checkout("Moby Dick")
  end
end